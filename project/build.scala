import sbt._
import sbt.Keys._

import sbt.LocalProject
import sbtassembly.Plugin._
import AssemblyKeys._
import scala.Some

object Dependencies {

  import Versions._

  val customResolvers = Seq(
    "Morphia Repository" at "http://morphia.googlecode.com/svn/mavenrepo/",
    "Typesafe Release Repository" at "http://repo.typesafe.com/typesafe/releases",
    "Typesafe Ivy Repository" at "http://repo.typesafe.com/typesafe/ivy-releases",
    "Scala Tools" at "https://oss.sonatype.org/content/groups/scala-tools"
  )

  val thirdPartyJavaLibraries = Seq(
    "org.apache.commons" % "commons-lang3" % "3.1",
    "org.slf4j" % "slf4j-api" % "1.6.6",
    "ch.qos.logback" % "logback-classic" % LOGBACK_VERSION,
    "ch.qos.logback" % "logback-core" % LOGBACK_VERSION,
    "org.easymock" % "easymock" % "3.0" % "test",
    "junit" % "junit" % "4.10" % "test",
    "org.hamcrest" % "hamcrest-all" % "1.1" % "test"
  )

  val thirdPartyScalaLibraries = Seq(
    "com.weiglewilczek.slf4s" %% "slf4s" % "1.0.7", // SLF4J wrapper
    "org.scalatest" %% "scalatest" % "1.8" % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % "2.2" % "test"
  )

}

object Versions {

  val LOGBACK_VERSION = "1.0.6"

}

object Settings {

  lazy val baseSettings = Defaults.defaultSettings ++ resourcesSettings ++ Seq(
    organization := "com.davezhu",
    scalaVersion := "2.9.1",
    crossScalaVersions := Seq("2.8.0", "2.8.1", "2.8.2", "2.9.0", "2.9.1"),
    scalacOptions += "-unchecked", // show warnings
    resolvers ++= Dependencies.customResolvers
  )

  lazy val resourcesSettings = Seq(resourceDirectory in Compile <<= baseDirectory { _ / "src/main/resources" },
    resourceDirectory in Test <<= baseDirectory { _ / "src/test/resources" })

}

object SAPIBuild extends Build {

  import Versions._

  import Settings._

  import Dependencies._

  val PROJECT = "chess"

  // Module definitions

  // Master module                                                          '
  lazy val rootModule = Project(PROJECT, file("."),
    settings = baseSettings ++ Seq(
      name := PROJECT + "-master",
      publish := {} // do not publish master
    ))

  // Core/Services module
  lazy val coreModule = module("core")(
    settings = Seq[Setting[_]](
      libraryDependencies ++= thirdPartyJavaLibraries ++ thirdPartyScalaLibraries
    )
  )

  private def srcPathSetting(projectId: String, rootPkg: String) : Setting[Task[Seq[(File, String)]]] = {
    mappings in (LocalProject(projectId), Compile,  packageSrc) ~= {
      defaults: Seq[(File, String)] =>
        defaults.map { case(file, path) =>
          (file, rootPkg + "/" + path)
        }
    }
  }

  private def module(name: String) (
    settings: Seq[Setting[_]],
    projectId: String = PROJECT + "-" + name,
    dirName: String = name,
    srcPath: String = PROJECT + name
  ) = Project(projectId, file(dirName), settings = (baseSettings ++ srcPathSetting(projectId, srcPath)) ++ settings)

}

