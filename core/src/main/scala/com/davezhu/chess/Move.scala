package com.davezhu.chess

import org.apache.commons.lang3.builder.HashCodeBuilder

trait Move extends Equals {

  val piece: Piece

  val to: Position

  override def hashCode() = new HashCodeBuilder().append(piece).append(to).toHashCode

  override def equals(o: Any) = {
    o match {
      case that: Move =>
        if (this eq that) {
          true
        } else {
          (that.## == this.##) && (that canEqual this) && (piece == that.piece) && (to == that.to)
        }
      case _ => false
    }
  }

  def canEqual(that: Any) = that.isInstanceOf[Move]

}

object Move {

  def apply(pc: Piece, ps: Position) = new Move {
    val piece = pc
    val to = ps
    override def toString = "Move(" + piece.toString + "," + to.toString + ")"
  }

  def unapply(m: Move): Option[(Piece, Position)] = {
    Some(m.piece, m.to)
  }

}

case class Capture(piece: Piece, captured: Piece, to: Position) extends Move

case class Promotion(piece: Piece, promotion: Piece, to: Position) extends Move

case class Check(piece: Piece, to: Position) extends Move

// check, checkmate, castle(queenside, kingside), enpassent?,
