package com.davezhu.chess

import Player.{Player, Black, White}

/**
 * Represents a game of chess
 * @author: dzhu
 */
class Game(val board: Board = Board(), val history: Seq[Move] = Seq(), val captures: Seq[Piece] = Seq()) {

  def pieceAt(pos: Position): Option[Piece] = board.get(pos)

  def historyOf(piece: Piece): Seq[Move] = Seq()

  // e.g. read as: "execute the move Qg5 at g2"
  // execute(move = Move(piece = Queen(White), to = Position('g', 5)), at = Position('g', 2))
  def execute(move: Move, at: Position): Option[Game] = {

    val newGame = board.get(at) match {

      case Some(piece) if (turn == piece.player) && (piece.moves(at, this) contains move) =>

        move match {

          case Capture(piece, captured, to) =>
            Some(new Game(new Board(board.setup - at + (to -> piece)), history :+ move, captures :+ captured))

          case Move(piece, to) =>
            Some(new Game(new Board(board.setup - at + (to -> piece)), history :+ move, captures))

          case _ => None

        }

      case _ => None

    }

    newGame.filter { !_.isChecked(move.piece.player) }

  }

  def turn: Player = {
    if (history.isEmpty) White
    else if (history.last.piece.player == White) Black
    else White
  }

  def isChecked(player: Player) = {

    val pieces = board.opponentPieces(player)

    pieces.exists {
      case (pos, piece) => {
        piece.moves(pos, this).exists {
          case Capture(_, King(player), _) => true
          case _ => false
        }
      }
    }

  }

  def isCheckmate = {

    !board.pieces(turn).exists {
      case (pos, piece) => piece.moves(pos, this).exists { move =>
          execute(move, pos).isDefined
      }
    }

  }

}

object Player extends Enumeration {

  type Player = Value

  val White = Value("W")
  val Black = Value("B")

}
