package com.davezhu.chess

import Player.{Player, White}

sealed trait Piece {

  val player: Player

  def moves(pos: Position, game: Game): Set[Move]

  /**
   * Returns true if the position occupied by friendly piece
   */
  def isOccupied(game: Game, pos: Position): Boolean = game.pieceAt(pos).exists(_.player == player)

}

case class Pawn(player: Player) extends Piece {

  def moves(current: Position, game: Game): Set[Move] = {

    val board = game.board

    val positions = if (game.historyOf(this).isEmpty) {
      project(board, current, 2)
    } else {
      project(board, current)
    }

    val advances = for (pos <- positions if board.get(pos).isEmpty) yield Move(this, pos)

    val captures = for (pos <- diagonal(current); piece <- board.get(pos) if piece.player != player) yield Capture(this, piece, pos)

    advances ++ captures

  }

  private def project(board: Board, current: Position, maxSteps: Int = 1): Set[Position] =
    if (player == White) board.project(current, N, maxSteps) else board.project(current, S, maxSteps)

  private def diagonal(pos: Position): Set[Position] = {

    if (player == White) {
      for (dir <- Set(NE, NW) if pos.possible(dir)) yield pos.move(dir)
    } else {
      for (dir <- Set(SE, SW) if pos.possible(dir)) yield pos.move(dir)
    }

  }

}

case class King(player: Player) extends Piece {

  def moves(pos: Position, game: Game): Set[Move] = {

    val positions =
      for (dir <- Direction.all; pos <- game.board.project(pos, dir, 1) if (!isOccupied(game, pos))) yield pos

    val captures = for (pos <- positions; capture <- game.pieceAt(pos)) yield Capture(this, capture, pos)

    val moves = (positions -- captures.map(_.to)).map(pos => Move(this, pos))

    captures ++ moves

  }

}

case class Rook(player: Player) extends Piece {
  def moves(pos: Position, game: Game) = Set()
}

case class Bishop(player: Player) extends Piece {
  def moves(pos: Position, game: Game) = Set()
}

case class Queen(player: Player) extends Piece {
  def moves(pos: Position, game: Game) = Set()
}

case class Knight(player: Player) extends Piece {
  def moves(pos: Position, game: Game) = Set()
}
