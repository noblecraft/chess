package com.davezhu.chess

import annotation.tailrec

import Board.{files, ranks}
import Player.{Player, Black, White}

object Board {

  val files = 'a' to 'h'

  val ranks = 1 to 8

  val initial = (files.map(f => Position(f, 1)) zip firstRank(White)) ++ files.map(f => Position(f, 2) -> Pawn(White)) ++
    (files.map(f => Position(f, 8)) zip firstRank(Black)) ++ files.map(f => Position(f, 7) -> Pawn(Black))

  def firstRank(p: Player): Seq[Piece] =
    Seq(Rook(p), Knight(p), Bishop(p), Queen(p), King(p), Bishop(p), Knight(p), Rook(p))

  def apply(pieces: (Position, Piece)*) = new Board(Map(pieces: _*))

  def apply() = new Board(Map(initial: _*))

}

/**
  * @author: dzhu
 */
class Board(val setup: Map[Position, Piece]) {

  def get(pos: Position): Option[Piece] = setup.get(pos)

  @tailrec final def project(from: Position, dir: Direction, maxSteps: Int, steps: Set[Position] = Set()): Set[Position] = {
    if (!from.possible(dir) || maxSteps == 0) {
      steps
    } else {
      val to = from.move(dir)
      setup.get(to) match {
        case Some(piece) => steps + to // stop here
        case _ => project(to, dir, maxSteps - 1, steps + to)
      }
    }
  }

  def opponentPieces(player: Player): Map[Position, Piece] = setup.filterNot(_._2.player == player)

  def pieces(player: Player): Map[Position, Piece] = setup.filter(_._2.player == player)

}

/**
 * Directional vector
 */
sealed abstract class Direction(val x: Int, val y: Int) extends (Int, Int)(x, y)

object N extends Direction(0, 1)
object S extends Direction(0, -1)
object E extends Direction(1, 0)
object W extends Direction(-1, 0)
object NE extends Direction(1, 1)
object NW extends Direction(-1, 1)
object SE extends Direction(1, -1)
object SW extends Direction(-1, -1)

object Direction {
  val all = Set(N, S, E, W, NE, NW, SE, SW)
}

case class Position(file: Char, rank: Int) {

  assert(files contains file.toLower)
  assert(ranks contains rank)

  def move(dir: Direction) = {
    assert(possible(dir))
    Position(files(deltaFile(dir)), rank + dir.y)
  }

  def possible(dir: Direction): Boolean = {
    val f = deltaFile(dir)
    val r = rank + dir.y
    (f >= 0 && f < files.size) && ranks.contains(r)
  }

  def deltaFile(dir: Direction): Int = files.indexOf(file) + dir.x

}

object Position {

  def apply(s: Symbol) = new Position(s.name(0), s.name(1).toString.toInt)

}
