package com.davezhu.chess

import org.scalatest.matchers.ShouldMatchers
import org.scalatest.{GivenWhenThen, FlatSpec}

import Player.{Player, Black, White}

/**
 * @author: dzhu
 */
class KingSpec extends FlatSpec with GivenWhenThen with ShouldMatchers {

  "King" should "be able to move one step in all directions" in {

    given("game with king @ (d2)")
    val king = King(White)
    val d2 = Position('d2)
    val game = new Game(Board(d2 -> king))

    then("it should be allowed to move to d3, c3, c2, c1, d1, e1, e2 or e3")
    val moves = king.moves(d2, game)
    moves should have size(8)
    moves should contain(Move(king, Position('d3)))
    moves should contain(Move(king, Position('c3)))
    moves should contain(Move(king, Position('c2)))
    moves should contain(Move(king, Position('c1)))
    moves should contain(Move(king, Position('d1)))
    moves should contain(Move(king, Position('e1)))
    moves should contain(Move(king, Position('e2)))
    moves should contain(Move(king, Position('e3)))

  }

  it should "not be able to move to a square that is occupied by a friendly piece" in {

    given("game with king @(d2)")
    val king = King(White)
    val d1 = Position('d1)
    val game = new Game(Board(d1 -> king, Position('c2) -> Pawn(White), Position('d2) -> Pawn(White),
        Position('e2) -> Pawn(White), Position('c1) -> Bishop(White), Position('e1) -> Queen(White)))

    then("king should not be able to make any moves")
    king.moves(d1, game) should be('empty)

  }

  it should "capture opponent pieces" in {

    given("game is setup with king @(d2), opponent pieces @ (c2), (d2), and a friendly piece @ (e2)")
    val king = King(White)
    val d1 = Position('d1)
    val game = new Game(Board(d1 -> king, Position('c2) -> Pawn(Black), Position('d2) -> Bishop(Black),
      Position('e2) -> Pawn(White)))

    then("king should have 4 moves available")
    val moves = king.moves(d1, game)

    moves should have size(4)
    and("king should be able to capture opponent pieces @ (c2), (d2)")
    moves should contain(Capture(king, Pawn(Black), Position('c2)): Move)
    moves should contain(Capture(king, Bishop(Black), Position('d2)): Move)
    and("king should be able to move to (c1) or (e1), but not (e2)")
    moves should contain(Move(king, Position('c1)))
    moves should contain(Move(king, Position('e1)))

  }

}
