package com.davezhu.chess

import org.scalatest.{GivenWhenThen, FlatSpec}
import org.scalatest.matchers.ShouldMatchers

import Player.{Player, Black, White}

/**
 * @author: dzhu
 */
class PawnSpec extends FlatSpec with ShouldMatchers with GivenWhenThen {

  "A Pawn" should "have 2 possible moves as its first move" in {

    given("a black pawn and a white pawn in its initial state - w(a2) b(a7)")
    val (a2, white) = Position('a2) -> Pawn(White)
    val (a7, black) = Position('a7) -> Pawn(Black)
    val game = new Game(Board(a2 -> white, a7 -> black))
    when("there are no pieces blocking in front of them")

    val m1 = white.moves(a2, game)
    val m2 = black.moves(a7, game)

    then("both pawns should have 2 moves available")
    m1 should have size(2)
    m2 should have size(2)

    and("the white pawn should be able to move to (a3) or (a4)")
    m1 should contain(Move(Pawn(White), Position('a3)))
    m1 should contain(Move(Pawn(White), Position('a4)))

    and("the black pawn should be able to move to (a6) or (a5)")
    m2 should contain(Move(Pawn(Black), Position('a6)))
    m2 should contain(Move(Pawn(Black), Position('a5)))

  }

  it should "not be able to advance 2 squares on its first move, if either square is occupied" in {

    given("a black pawn and a white pawn in its initial state - w(a2) b(a7)")
    val (a2, white) = Position('a2) -> Pawn(White)
    val (a7, black) = Position('a7) -> Pawn(Black)
    and("there is a black piece @ (a3), and a black piece @ (a5)")
    val game = new Game(Board(a2 -> white, a7 -> black, Position('a3) -> Bishop(Black), Position('a5) -> Rook(Black)))

    then("the white pawn is unable to move => blocked by black piece @ (a3)")
    white.moves(a2, game) should be('empty)

    and("the black pawn can only advance one square from (a7) to (a6) => blocked by black piece @ (a5)")
    val blackMoves = black.moves(a7, game)
    blackMoves should have size(1)
    blackMoves should contain(Move(Pawn(Black), Position('a6)))

  }

  it should "not be able to move to a square occupied by a friendly piece" in {

    given("game is setup with white pawn @ (a2), a friendly piece @ (a3) and a black piece @ (b3)")
    val (a2, white) = Position('a2) -> Pawn(White)
    val (b3, black) = Position('b3) -> Bishop(Black)
    val game = new Game(Board(a2 -> white, b3 -> black, Position('a3) -> Rook(White)))

    then("the pawn @(a2) should have 1 move available")
    val moves = white.moves(a2, game)
    moves should have size(1)

    and("it should be to capture the black piece @ (b3)")
    moves should contain(Capture(white, black, b3): Move)

  }

  it should "be able to capture opponent pieces" in {

    given("an opposing piece that is one square diagonally in front of the pawn")
    val (b2, white) = Position('b2) -> Pawn(White)

    and("a friendly piece that is one square diagonally in front of the pawn")
    val game = new Game(Board(b2 -> white, Position('a3) -> Rook(Black), Position('c3) -> Pawn(White)))

    val moves = white.moves(b2, game)
    then("the pawn should have 3 moves available")
    moves should have size(3)

    and("one of the moves available to the pawn is to capture the opposing piece")
    moves should contain(Capture(white, Rook(Black), Position('a3)) : Move)

    and("the only other moves available to the pawn is to advance to (b3) or to (b4)")
    moves should contain(Move(white, Position('b3)))
    moves should contain(Move(white, Position('b4)))

    and("the pawn should not be able to capture the friendly piece")
    moves should not contain(Capture(white, Pawn(White), Position('c3)))

  }

  it should "only be able to move forward" in(pending)

  it should "execute en passant move" in(pending)

}
