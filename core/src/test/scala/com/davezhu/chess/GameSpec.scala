package com.davezhu.chess

import org.scalatest.{GivenWhenThen, FlatSpec}
import org.scalatest.matchers.ShouldMatchers

import Player.{Player, Black, White}

/**
 * @author: dzhu
 */
class GameSpec extends FlatSpec with ShouldMatchers with GivenWhenThen {

  "A chess game" should "be setup with all pieces in its initial position" in {

    val game = new Game

    game.pieceAt(Position('a1)) should be(Some(Rook(White)))
    game.pieceAt(Position('b1)) should be(Some(Knight(White)))
    game.pieceAt(Position('c1)) should be(Some(Bishop(White)))
    game.pieceAt(Position('d1)) should be(Some(Queen(White)))
    game.pieceAt(Position('e1)) should be(Some(King(White)))
    game.pieceAt(Position('f1)) should be(Some(Bishop(White)))
    game.pieceAt(Position('g1)) should be(Some(Knight(White)))
    game.pieceAt(Position('h1)) should be(Some(Rook(White)))

    for (file <- 'a' to 'h') game.pieceAt(Position(file, 2)) should be(Some(Pawn(White)))

    game.pieceAt(Position('a8)) should be(Some(Rook(Black)))
    game.pieceAt(Position('b8)) should be(Some(Knight(Black)))
    game.pieceAt(Position('c8)) should be(Some(Bishop(Black)))
    game.pieceAt(Position('d8)) should be(Some(Queen(Black)))
    game.pieceAt(Position('e8)) should be(Some(King(Black)))
    game.pieceAt(Position('f8)) should be(Some(Bishop(Black)))
    game.pieceAt(Position('g8)) should be(Some(Knight(Black)))
    game.pieceAt(Position('h8)) should be(Some(Rook(Black)))

    for (file <- 'a' to 'h') game.pieceAt(Position(file, 7)) should be(Some(Pawn(Black)))

    for (f <- 'a' to 'h'; r <- 3 to 6) game.pieceAt(Position(f, r)) should be(None)

  }

  it should "allow player to execute any moves available to a piece" in {

    given("a game with a black and a white pawn @ (a2), (a7) respectively")
    val white = Pawn(White)
    val black = Pawn(Black)
    val g0 = new Game(Board(Position('a2) -> white, Position('a7) -> black))

    when("player attempts to move the white pawn from (a2) to (a4)")
    then("the move should be executed")
    val g1 = g0.execute(Move(white, Position('a4)), Position('a2)).get
    and("the move should be recorded in game's history")
    g1.history should have size(1)
    g1.history.last should be (Move(white, Position('a4)))

    when("player attempts to move the black pawn from (a7) to (a6)")
    val g2 = g1.execute(Move(black, Position('a6)), Position('a7)).get
    then("the move should be executed")
    g2.history should have size(2)
    and("the move should be recorded in game's history")
    g2.history.last should be (Move(black, Position('a6)))

  }

  it should "allow players to make moves in turn" in {

    given("a game is setup")
    val g0 = new Game

    then("the white player should be allowed to make the first move")
    g0.execute(Move(Pawn(Black), Position('a7)), Position('a6)) should be(None)
    val g1 = g0.execute(Move(Pawn(White), Position('a3)), Position('a2)).get

    and("the black player should be allowed to make the next move")
    g1.execute(Move(Pawn(White), Position('a4)), Position('a5)) should be(None)
    val g2 = g1.execute(Move(Pawn(Black), Position('a6)), Position('a7)).get

    and("the white player should be allowed to make the next move and so on...")
    g2.execute(Move(Pawn(Black), Position('a5)), Position('a6)) should be(None)
    g2.execute(Move(Pawn(White), Position('b3)), Position('b2)) should be('defined)

  }

  it should "allow a player to capture an opponent's piece" in {

    val (a2, b3) = (Position('a2), Position('b3))

    given("a game is setup with a white pawn in a position to capture a black piece")
    val g0 = new Game(
      board = Board(a2 -> Pawn(White), b3 -> Bishop(Black)),
      history = Seq(Move(Bishop(Black), b3))
    )
    when("white executes move to capture the black piece")
    val g1 = g0.execute(Capture(Pawn(White), Bishop(Black), b3), a2).get

    then("the black piece should be captured")
    g1.captures should have size(1)
    g1.captures.last should be(Bishop(Black))

    and("the captured piece should not be on the board anymore")
    g1.board.get(b3) should be(Some(Pawn(White)))
    g1.board.get(a2) should be(None)

  }

  it should "not allow any moves that will allow the opponent to immediately check the player's king" in {

    given("game is setup with king @(a1), opponent pawn @ (a2) and (b2)")
    val (a1, king) = Position('a1) -> King(White)
    val (a2, pawn1) = Position('a2) -> Pawn(Black)
    val (b2, pawn2) = Position('b2) -> Pawn(Black)
    val (b3, pawn3) = Position('b3) -> Pawn(Black)
    val g0 = new Game(Board(a1 -> king, a2 -> pawn1, b2 -> pawn2, b3 -> pawn3))

    then("the king should have only 1 move available - to capture the pawn @ (b2)")
    g0.execute(Capture(king, pawn2, b2), a1) should not be(None)
    and("the king should NOT be able to move to (b1) - check by pawn @ (a2)")
    g0.execute(Move(king, Position('b1)), a1) should be(None)
    and("the king should NOT be able to capture pawn @ (a2) - check by pawn @ (b3)")
    g0.execute(Capture(king, pawn1, a2), a1) should be(None)

  }

  it should "end the game when checkmate" in {

    given("a game in which white can not escape from a check by black")
    val game = new Game(Board(Position('a1) -> King(White), Position('a2) -> Pawn(Black), Position('a3) -> Pawn(Black),
      Position('b2) -> Pawn(Black), Position('b3) -> Pawn(Black)))

    then("game should be in checkmate (black wins)")
    game.isCheckmate should be(true)

  }

  it should "not end the game when king is able to escape check" in {

    given("a game in which white can escape from a check by black")
    val game = new Game(
      board = Board(Position('a1) -> King(White), Position('a2) -> Pawn(Black), Position('b2) -> Pawn(Black))
    )

    then("game should not be in checkmate (black wins)")
    game.isCheckmate should be(false)

  }

}
